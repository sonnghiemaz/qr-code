
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:qr_flutter/qr_flutter.dart';

class GenerateView extends StatefulWidget {
  @override
  _GenerateViewState createState() => _GenerateViewState();
}

class _GenerateViewState extends State<GenerateView> {
  final controller = TextEditingController();
  GlobalKey globalKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      body: Center(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RepaintBoundary(
                key: globalKey,
                child: QrImage(
                  data: controller.text,
                  size: 300,
                  backgroundColor: Colors.white,
                ),
              ),
              SizedBox(height: 40),
              buildTextField(),
            ],
          ),
        ),
      ),
    );
    }

  Widget buildTextField() {
    return TextField(
      controller: controller,
      style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 20
      ),
      decoration: InputDecoration(
          hintText: 'Enter the data',
          hintStyle: TextStyle(color: Colors.grey),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: Colors.white)
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide(color: Colors.greenAccent)
          ),
          suffixIcon: IconButton(
            color: Colors.greenAccent,
            onPressed: () => setState(() {}),
            icon: Icon(Icons.done, size: 30),
          )
      ),
    );
  }
  }
