import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qrcode/generate_view.dart';
import 'package:qrcode/scan_view.dart';

void main() => runApp(const MaterialApp(
    home: MyHome(),
    debugShowCheckedModeBanner: false,
));

class MyHome extends StatelessWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('QR Scanner Demo')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) =>  ScanView(),
                ));
              },
              child: const Text('Scan'),
            ),
            SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => GenerateView(),
                ));
              },
              child: const Text('Generate'),
            ),
          ],
        ),
      ),
    );
  }
}


